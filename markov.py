#!/usr/bin/env python3
import numpy as np
import itertools


class MarkovChain(object):
    def __init__(self, transition_table, states):
        """Método de iniciailización de la cadena de Markov

        Args:
            transition_table (np.array): Array con las probabilidades
                                     de transición
            states (list, optional): Estados de la cadena, se usan para
                                     relacionar índices a estados
        """
        self._transition_table = np.array(transition_table)
        self._states = list(states)

    def next_state(self, current_state):
        """Obtiene el siguiente estado de la cadena dado el estado actual

        Args:
            current_state (str): El estado actual
        """
        index_current = self._states.index(current_state)
        return np.random.choice(
            self._states,
            p=[self._transition_table[index_current][self._states.index(state)]
                for state in self._states])

    def generate_states(self, current_state, qty=10):
        """Genera múltiples estado siguientes

        Args:
            current_state (str): El estado actual
            qty (int, optional): Cantidad de estados a generar
        """
        future_states = []
        for i in range(qty):
            next_state = self.next_state(current_state)
            future_states.append(next_state)
            current_state = next_state
        return future_states


def infer_probabilities(text, delimiter='.'):
    """Infiere las probabilidades y construye el grafo a partir de un texto

    Args:
        text (str): Texto a partir del cual se entrenará la cadena de Markov
        delimiter (str, optional): Carácter delimitador, el texto se dividirá
                                   por este carácter.
    """
    sequences = list(map(str.strip, text.split(delimiter)))
    with open('sequences.txt', 'w') as outputf:
        outputf.writelines(['{}\n'.format(s) for s in sequences])
    if '' in sequences:
        sequences.remove('')
    states = sorted(set(text.lower().replace(delimiter, '').split(' ')))
    states = ['start'] + states + ['end']
    with open('states.txt', 'w') as outputf:
        outputf.writelines(['{}\n'.format(s) for s in states])
    number_states = len(states)
    probabilities = np.zeros((number_states, number_states))
    word_graph = dict(
        itertools.zip_longest(states,
                              [set({}) for i in range(number_states)]))
    for seq in sequences:
        words = seq.split(' ')
        for index, word in enumerate(words):
            word_index = states.index(word.lower())
            if index == 0:
                current_state = 0
                current_word = 'start'
            probabilities[current_state][word_index] += 1.
            word_graph[current_word.lower()].add(word)
            current_state = word_index
            current_word = word
    # Normalizar las probabilidades
    for i in range(number_states):
        if probabilities[i].sum() == 0:
            probabilities[i] = np.zeros(number_states)
        else:
            probabilities[i] = probabilities[i] / probabilities[i].sum()
    return probabilities, states
