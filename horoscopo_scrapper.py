import requests
from bs4 import BeautifulSoup
import time
import urllib.request
import csv
import time
import json
import datetime


# Apple iPhone XS con Chrome
USER_AGENT = 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/69.0.3497.105 Mobile/15E148 Safari/605.1'
SIGNOS = ['aries', 'tauro', 'geminis', 'cancer', 'leo', 'virgo', 'libra', 'escorpio', 'sagitario', 'capricornio', 'acuario', 'piscis']


def save_to_csv(data, output='default.csv'):
    """Guarda una lista de diccionarios a un archivo CSV

    Args:
        data (list): La lista con los datos a guardar
        output (str, optional): Ruta al archivo para guardar

    Returns:
        None
    """
    if len(data) == 0:
        return
    with open(output, 'w') as csvfile:
        cwriter = csv.DictWriter(csvfile, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL, fieldnames=data[0].keys())
        cwriter.writeheader()
        for row in data:
            cwriter.writerow(row)


def load_from_csv(filename):
    """Carga un archivo CSV desde un sistema de archivos

    Args:
        filename (str): Ruta del archivo a leer

    Returns:
        list: Lista de diccionarios con el contenido del archivo
    """
    data = []
    with open(filename, 'r') as csvfile:
        creader = csv.DictReader(csvfile)
        for row in creader:
            data += [row]
    return data


def debate_get_horoscopo_link(size=15):
    """Obtiene las urls de la páginas con horóscopos
    de debate.com.mx

    Args:
        size (int, optional): Cantidad de urls a obtener

    Returns:
        list: Una lista con los diccionarios por cada URL
    """
    values = []
    base_url = 'https://www.debate.com.mx/ajax/get_tags_news.html'
    params = {'tags': 'horoscopo-de-hoy',
              'page': 1,
              'size': size,
              'publication': 8}
    headers = {'User-Agent': USER_AGENT}
    page = requests.get(base_url, params=params, headers=headers)
    soup = BeautifulSoup(page.content, 'html.parser')
    links = soup.find_all('figure')
    for link in links:
        a = link.find('a')
        values += [{'url': a['href'], 'title': a['title']}]

    return values


def debate_get_horoscopo(url, fecha):
    """Obtiene el texto de una URL de debate.com.mx
    y parsea el contenido

    Args:
        url (str): URL A consultar
        fecha (datetime): Día para el cual fueron escritos los horóscopos

    Returns:
        list: Diccionario incluyendo el texto de los horóscopos
    """
    data = []
    headers = {'User-Agent': USER_AGENT}
    page = requests.get(url, headers=headers)
    print("Got response {} ".format(page.status_code))
    soup = BeautifulSoup(page.content, 'html.parser')
    scripts = soup.find_all('script')
    print('{} script tags'.format(len(scripts)))
    for script in scripts:
        print('{}\n'.format(script.string))
        try:
            jsondata = json.loads(script.string)
            print('JSON parsed')
            if 'itemListElement' in jsondata:
                for horoscopo in jsondata['itemListElement']:
                    title = horoscopo['item']['headline']
                    content = horoscopo['item']['description']
                    data += [{
                            'signo': title,
                            'texto': content,
                            'fecha': fecha,
                            'proveedor': 'debate.com.mx'
                            }]
                print('Extracted: {}'.format(title))
            else:
                print('Does not have a itemListElement')
        except:
            print('Invalid JSON')
            continue
    return data


def debate_scrapper():
    """Punto de entrada para scrappear en debate.com.mx
    Obtiene las URLs desde un archivo CSV y guarda los resultados
    a un archivo CSV
    """
    all_horoscopos = []
    # save_to_csv(debate_get_horoscopo(365), 'debate_horoscopo.csv')
    registros = load_from_csv('debate_horoscopo.csv')
    for dia in registros:
        horoscopos_dia = debate_get_horoscopo(dia['url'], dia['date'])
        print("Recuperados {} horoscopos en el dia {}".format(
            len(horoscopos_dia), dia['date']))
        all_horoscopos += [horoscopos_dia]
        time.sleep(10)
    save_to_csv(all_horoscopos, 'debate_todos_horoscopos.csv')


def get_abc_urls(qty, start):
    """Generar las URLs de consulta de horóscopos en
    abc.es

    Args:
        qty (int, optional): Cantidad de días a considerar
        start (str): Cadena de formato %d%m%Y indicando la fecha inicial

    Returns:
        list: Lista con diccionarios por cada URL, especificando
        el signo y la fecha
    """
    all_links = []
    url = "https://horoscopo.abc.es/signos-zodiaco-{sig}/prediccion-{day}.html"
    initial_date = datetime.datetime.strptime(start, '%d%m%Y')
    for dayoffset in range(qty):
        day = initial_date + datetime.timedelta(days=dayoffset)
        urls = [{'url': url.format(sig=x, day=day.strftime('%d%m%Y')),
                 'fecha': day.strftime('%Y-%m-%d'),
                 'signo': x} for x in SIGNOS]
        all_links = all_links + urls

    return all_links


def get_abc_contents(url, fecha, signo):
    """Lee el contenido de una URL y obtiene el texto
    del horóscopo que se encuentre en ella

    Args:
        url (str): URL del horóscopo
        fecha (datetime): Fecha del horóscopo
        signo (str): Signo del horóscopo

    Returns:
        list: Lista de diccionarios con el contenido de los horóscopos
    """
    data = []
    headers = {'User-Agent': USER_AGENT}
    page = requests.get(url, headers=headers)
    if page.status_code != 200:
        print('Error {} receiving page: {}'.format(page.status_code, url))
        return data

    soup = BeautifulSoup(page.content, 'html.parser')
    txtNews = soup.find('div', class_='inside')
    if txtNews:
        text = txtNews.find_all('p')[8].text
        data.append({
            'url': url,
            'fecha': fecha.strftime('%Y-%m-%d'),
            'signo': signo,
            'texto': text})
    else:
        print('Error: no content in page {}'.format(url))
    return data


def abc_scrapper():
    """Punto de inicio para scrappear en el servidor abc.es
    """
    db = []
    links = get_abc_urls(365, '04082018')
    save_to_csv(links, 'abc_es_urls.csv')
    # links = load_from_csv('abc_es_urls.csv')
    i = 1
    total = len(links)
    for page in links:
        print("Scrapping url {}/{}".format(i, total))
        txt = get_abc_contents(page['url'],
                               datetime.datetime.fromisoformat(page['fecha']),
                               page['signo'])
        db = db + txt
        i = i + 1
        time.sleep(5)

    print(db)
    save_to_csv(db, 'abc.es_04082018-04082019.csv')


def main():
    abc_scrapper()


if __name__ == '__main__':
    main()
